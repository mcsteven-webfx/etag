<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <!-- font preconnect -->
    <link href="https://fonts.googleapis.com/css2?family=Lexend:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <!-- font-family: 'Lexend', sans-serif; -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <!-- font-family: 'Montserrat', sans-serif; -->
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <!-- font-family: 'Jost', sans-serif; -->

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <?php wp_body_open(); ?>

    <?php
        //Theme Settings Variable
        $logo_id    = fx_get_client_logo_image_id();
        $home_url   = get_home_url();
    ?>

    <header class="page-header">
        <div class="container clearfix">
            <div class="logo">
                <a href="<?php echo $home_url; ?>">
                    <?php echo fx_get_image_tag( 'https://etagtechnologies.webpagefxstage.com/wp-content/uploads/2022/02/logo.png', 'img-responsive' ); ?>
                </a>
            </div>
            <?php ubermenu( 'main' ); ?>
            <div class="header-right">
                <div class="sign-in"><a href="#"> <i class="icon-Sign-In"></i> Sign In </a></div>
                <div class="header-btn hidden-xs-down"><a href="#" class="btn btn-primary">SCHEDULE A DEMO</a></div>
                <?php ubermenu_toggle(); ?>
                <div class="search-icon js-search-toggle hidden-sm hidden-md"><i class="icon-Search-1"></i></div>
            </div>
        </div>
        <div class="nav-fixed hidden-sm-up">
            <div class="fixed-btn"><a href="#" class="btn btn-primary">SCHEDULE A DEMO</a></div>
            <?php ubermenu_toggle(); ?>
        </div>
        <?php get_search_form(); ?>
    </header>
