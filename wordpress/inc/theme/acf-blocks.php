<?php

/**
 * Register FX blocks
 *
 * fx_register_block() is, at its core, a wrapper function for acf_register_block_type with additional parameters for
 * our supporting functionality
 *
 * @see Guru card: https://app.getguru.com/card/Tn9zzk8c/FX-ACF-Blocks
 * @see more info for acf_register_block_type(): https://www.advancedcustomfields.com/resources/acf_register_block_type/
 *
 */

/**
 * General blocks
 *
 * These blocks are intended to be used anywhere, including the homepage and innerpage.
 *
 * Block template path: /themes/fx/block-templates/general
 * Stylesheet path:     /themes/fx/assets/css/general
 * Script path:         /themes/fx/assets/js/general
 *
 */


/**
 * Create a "FX General Blocks" category in the block editor. Use "fx-general-blocks" as your "category" value in
 * fx_register_block()
 *
 */
fx_add_block_category( 'FX General Blocks', 'fx-general-blocks' );


/**
 * Plain WYSIWYG block for general usage
 *
 */
fx_register_block(
    [
        'name'          => 'wysiwyg',
        'title'         => 'WYSIWYG',
        'template'      => 'general/wysiwyg.php',
        'description'   => 'A basic "What you see is what you get" editor.',
        'css_deps'      => [ 'fx_wysiwyg' ],
        'post_types'    => [],
    ]
);


/**
 * To avoid issues with CF7 assets, we're creating our own CF7 block. You shouldn't need to touch this section.
 *
 */
$cf7_settings = [
    'name'          => 'cf7-block',
    'title'         => 'CF7 Block',
    'template'      => 'general/cf7-block.php',
    'description'   => 'Adds a CF7 block to page',
    'css'           => 'innerpage/contact.css',
    'css_deps'      => [ 'fx_choices_custom', 'contact-form-7' ],
    'js_deps'       => [ 'contact-form-7', 'wpcf7-recaptcha', 'google-recaptcha' ],
    'keywords'      => [ 'cf7', 'contact', 'form' ],
    'mode'          => 'edit',
    'post_types'    => [], // all post types
];
$cf7_icon = WP_PLUGIN_DIR . '/contact-form-7/assets/icon.svg';
if( file_exists( $cf7_icon ) ) {
    $cf7_settings['icon'] = file_get_contents( $cf7_icon );
}
fx_register_block( $cf7_settings );

/**
 * Homepage blocks
 *
 * These blocks are intended to be used ONLY on the homepage.
 *
 * Block template path: /themes/fx/block-templates/homepage
 * Stylesheet path:     /themes/fx/assets/css/homepage
 * Script path:         /themes/fx/assets/js/homepage
 *
 */

/**
 * Create a "FX Homepage Blocks" category in the block editor. Use "fx-homepage-blocks" as your "category" value in
 * fx_register_block()
 *
 */
fx_add_block_category( 'FX Homepage Blocks', 'fx-homepage-blocks' );


/**
 * This is the main homepage "outer block." All other homepage blocks should be added within this block in the Block
 * Editor and in block-templates/homepage/homepage-block.php
 *
 */
fx_register_block(
    [
        'name'          => 'homepage-block',
        'title'         => 'Homepage',
        'template'      => 'homepage/homepage-block.php',
        'description'   => 'The main content block for the homepage',
        'mode'          => 'preview',
        'supports'      => [ 'jsx' => true ], // enables support for inner blocks
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-masthead-slider',
        'title'         => 'Homepage - Masthead Slider',
        'template'      => 'homepage/masthead-slider.php',
        'description'   => 'Slider block for the homepage masthead.',
        'css'           => 'homepage/masthead.css',
        'css_deps'      => [ 'fx_slick' ],
        'js'            => 'homepage/masthead-slider.js',
        'js_deps'       => [ 'fx_slick' ],
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-quick-info',
        'title'         => 'Homepage - Quick Info',
        'template'      => 'homepage/quick-info.php',
        'description'   => 'Quick info block for the homepage.',
        'css'           => 'homepage/quick-info.css',
        'js'            => 'homepage/quick-info.js',
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-product',
        'title'         => 'Homepage - Product',
        'template'      => 'homepage/product.php',
        'description'   => 'Product block for the homepage.',
        'css'           => 'homepage/product.css',
        'css_deps'      => [ 'fx_slick' ],
        'js'            => 'homepage/product-slider.js',
        'js_deps'       => [ 'fx_slick' ],
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-image-buttons',
        'title'         => 'Homepage - Image Buttons',
        'template'      => 'homepage/image-buttons.php',
        'description'   => 'Image buttons block for the homepage.',
        'css'           => 'homepage/industry.css',
        'css_deps'      => [ 'fx_slick' ],
        'js'            => 'homepage/industrial-slider.js',
        'js_deps'       => [ 'fx_slick' ],
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-brand-logos',
        'title'         => 'Homepage - Brand Logos',
        'template'      => 'homepage/brand-logos.php',
        'description'   => 'Brand Logos block for the homepage.',
        'css'           => 'homepage/brand.css',
        'css_deps'      => [ 'fx_slick' ],
        'js'            => 'homepage/brand-slider.js',
        'js_deps'       => [ 'fx_slick' ],
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-testimonial',
        'title'         => 'Homepage - Testimonials',
        'template'      => 'homepage/testimonial.php',
        'description'   => 'Testimonial block for the homepage.',
        'css'           => 'homepage/testimonials.css',
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-process',
        'title'         => 'Homepage - Process',
        'template'      => 'homepage/process.php',
        'description'   => 'Process block for the homepage.',
        'css'           => 'homepage/process.css',
        'css_deps'      => [ 'fx_readmore', 'fx_slick' ],
        'js'            => 'homepage/process-slider.js',
        'js_deps'       => [ 'fx_readmore', 'fx_slick' ],
        'category'      => 'fx-homepage-blocks',
    ]
);

/**
 * Innerpage blocks
 *
 * These blocks are intended to be used ONLY on innerpages
 *
 * Block template path: /themes/fx/block-templates/innerpage
 * Stylesheet path:     /themes/fx/assets/css/innerpage
 * Script path:         /themes/fx/assets/js/innerpage
 *
 */

/**
 * Create a "FX Innerpage Blocks" category in the block editor. Use "fx-innerpage-blocks" as your "category" value in
 * fx_register_block()
 *
 */
fx_add_block_category( 'FX Innerpage Blocks', 'fx-innerpage-blocks' );

fx_register_block(
    [
        'name'          => 'innerpage-half-half',
        'title'         => 'Innerpage - Half and Half',
        'template'      => 'innerpage/half-half.php',
        'description'   => 'Half and Half block for the innerpages.',
        'css_deps'      => ['fx_half_image_text', 'fx_wysiwyg'],
        'category'      => 'fx-innerpage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'innerpage-cta',
        'title'         => 'Innerpage - Call To Action',
        'template'      => 'innerpage/cta.php',
        'description'   => 'Call to action block for the innerpages.',
        'css'           => 'homepage/cta.css',
        'category'      => 'fx-innerpage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'innerpage-image-button',
        'title'         => 'Innerpage - Image Buttons',
        'template'      => 'innerpage/image-buttons.php',
        'description'   => 'Image buttons block for the innerpages.',
        'css'           => 'innerpage/image-buttons.css',
        'css_deps'      => [ 'fx_slick' ],
        'js'            => 'innerpage/image-button-slider.js',
        'js_deps'       => [ 'fx_slick' ],
        'category'      => 'fx-innerpage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'innerpage-faq',
        'title'         => 'Innerpage - Frequently Asked Questions',
        'template'      => 'innerpage/faq.php',
        'description'   => 'Frequently asked questions block for the innerpages.',
        'css_deps'      => [ 'fx_accordion' ],
        'js'            => 'innerpage/faq.js',
        'js_deps'       => [ 'fx_accordion' ],
        'category'      => 'fx-innerpage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'innerpage-cards',
        'title'         => 'Innerpage - Cards',
        'template'      => 'innerpage/cards.php',
        'css'           => 'innerpage/cards.css',
        'description'   => 'Cards block for the innerpages.',
        'category'      => 'fx-innerpage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'innerpage-testimonial',
        'title'         => 'Innerpage - Testimonials',
        'template'      => 'innerpage/testimonial.php',
        'css'           => 'homepage/testimonials.css',
        'css_deps'      => [ 'fx_slick' ],
        'js'            => 'innerpage/testimonials.js',
        'js_deps'       => [ 'fx_slick' ],
        'description'   => 'Testimonial block for the innerpages.',
        'category'      => 'fx-innerpage-blocks',
    ]
);
