var FX = ( function( FX, $ ) {


	$( () => {
		FX.AccordionHeight.init();
	})

    FX.AccordionHeight = {
		init() {
			$('.js-accordion-item').each(function(){
				var height = $(this).find('.fx-accordion__panel__content p').height();
				$(this).find('.fx-accordion__panel__content').css('height', height);
			});
		},
	};

	return FX

} ( FX || {}, jQuery ) )
