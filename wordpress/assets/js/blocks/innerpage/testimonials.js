var FX = ( function( FX, $ ) {


	$( () => {
		FX.TestimonialsSlider.init()
	})


	FX.TestimonialsSlider = {
		$slider: null,

		init() {
			$('.js-testimonials-slider').slick({
				infinite: true,
				speed: 300,
				dots: false,
				arrows: true,
				slidesToShow: 1,
			});	


		},

		applySlick() {
            this.$slider.slick( {
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
            });
		}
	}

	

	return FX

} ( FX || {}, jQuery ) )