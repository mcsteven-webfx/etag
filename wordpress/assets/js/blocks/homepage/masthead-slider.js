var FX = ( function( FX, $ ) {


	$( () => {
		FX.MastheadSlider.init()
	})


	FX.MastheadSlider = {
		$slider: null,

		init() {
			$('.masthead-slider').slick({
				dots: false,
				infinite: true,
				speed: 300,
				slidesToShow: 1,
				variableWidth: false,
				arrows: false,
			});
		},

		applySlick() {
            this.$slider.slick( {
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
            });
		}
	}



	return FX

} ( FX || {}, jQuery ) )
