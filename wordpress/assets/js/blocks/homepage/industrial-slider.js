var FX = ( function( FX, $ ) {


	$( () => {
		FX.IndustrialSlider.init()
	})


	FX.IndustrialSlider = {
		$slider: null,

		init() {
			$('.industry-slider').slick({
				dots: false,
				infinite: true,
				speed: 300,
				slidesToShow: 1,
				variableWidth: true,
				arrows: false,
				draggable: false,
				responsive: [
					{
					  breakpoint: 1200,
					  settings: 'unslick'
					}
				  ]
			});


		},

		applySlick() {
            this.$slider.slick( {
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
            });
		}
	}



	return FX

} ( FX || {}, jQuery ) )
