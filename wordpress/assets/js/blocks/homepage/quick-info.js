var FX = ( function( FX, $ ) {


	$( () => {
		FX.QuickInfoAnimation.init()
	})


	FX.QuickInfoAnimation = {
        $parent: null,
		$counter: null,

		init() {
			this.$parent = $('.quick-info');
			this.$counter = 0;

			$(window).on( 'load', this.isInViewport.bind( this ) );

			$(window).on( 'resize scroll', this.isInViewport.bind( this ) );
		},

		isInViewport() {
			var top_element = this.$parent.offset().top;
		    var bottom_element = this.$parent.offset().top + this.$parent.outerHeight();
		    var bottom_screen = $(window).scrollTop() + $(window).innerHeight();
		    var top_screen = $(window).scrollTop() + 100;

		    if ((bottom_screen > top_element) && (top_screen < bottom_element) && this.$counter == 0){
				$('.quick-info-count').each(function () {
					$(this).prop('Counter', 0).animate({
							Counter: $(this).data('value')
						}, {
						duration: 2000,
						easing: 'swing',
						step: function (now) {
							$(this).text(this.Counter.toFixed(0));
						}
					});
				});
				this.$counter = 1;
		    }
		}
	}



	return FX

} ( FX || {}, jQuery ) )
