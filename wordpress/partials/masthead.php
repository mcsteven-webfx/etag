<!-- Innerpage Masthead -->
<section class="masthead masthead--inner">
    <div class="masthead-img">
        <?php echo fx_get_image_tag( 'https://etagtechnologies.webpagefxstage.com/wp-content/uploads/2022/02/inner-banner.png', 'img-responsive' ); ?>
    </div>
    <div class="masthead__innercontent">
        <div class="container">
            <div class="masthead-inner__text text-center">
                <?php if ( is_search() ): ?>
                    <h3 class="h1">Search Results</h3><?php /* different heading type for SEO benefit */ ?>
                <?php elseif ( is_home() ): ?>
                    <h1>Blog</h1><?php /* different heading type for SEO benefit */ ?>
                <?php elseif ( is_404() ) : ?>
                    <h1>404: Oops! We can't find the page you're looking for.</h1>
                <?php elseif ( is_archive() ): ?>
                    <h1><?php echo get_the_archive_title(); ?></h1>
                <?php elseif ( is_single() ): ?>
                    <h4 class="h1"><?php the_title(); ?></h4>
                <?php else : ?>
                    <h1><?php the_title(); ?></h1>
                <?php endif; ?>

                <div class="breadcrumbs hidden-xs-down">
                    <?php
                        if( function_exists( 'yoast_breadcrumb' ) ) {
                            yoast_breadcrumb( '<ul class="clearfix">', '</ul>' );
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Innerpage Masthead End -->
