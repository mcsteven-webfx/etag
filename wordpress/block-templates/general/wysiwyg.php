<!-- WYSIWYG -->
<?php
	//WYSIWYG Variables
	$bg      = get_field('background');
	$content = get_field('content');
?>
<section class="wysiwyg <?php echo $bg; ?> section-padding">
	<div class="wysiwyg-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					<?php echo $content; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if( $bg == 'pattern-wysiwyg'): ?>
		<div class="pattern-wysiwyg-bg">
			<?php echo fx_get_image_tag('https://etagtechnologies.webpagefxstage.com/wp-content/uploads/2022/02/pattern-wysiwyg.png', 'img-responsive'); ?>
		</div>
	<?php endif; ?>
</section>
<!-- WYSIWYG End -->
