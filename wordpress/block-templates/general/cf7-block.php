<!-- Innerpage Contact -->
<section class="page-contact section-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <?php
                    //Contact Variables
                    $title       = get_field('title');
                    $description = get_field('description');
                    $form        = get_field('cf7_shortcode');
                ?>
                <div class="contact-details">
                    <h2><?php echo $title; ?></h2>
                    <p><?php echo $description; ?></p>
                    <div class="contact-form">
                        <?php
                            if( !empty( $form ) ) {
                                echo apply_shortcodes( $form );
                            }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="sidebar">
                    <div class="sidebar__title">
                        <h4>Contact Us Directly</h4>
                    </div>
                    <div class="sidebar__text">
                        <ul>
                            <?php
                                //Site Details Variables
                                $address    = fx_get_client_address();
                                $email      = fx_get_client_email( true );
                                $phone      = fx_get_client_phone_number();
                                $phone_link = fx_get_client_phone_number( true );
                                $fax        = get_field('fax', 'options');
                            ?>
                            <li><span>Address</span><?php echo $address; ?></li>
                            <li><span>Phone</span><a href="tel:<?php echo $phone_link; ?>"><?php echo $phone; ?></a></li>
                            <li><span>Fax</span><?php echo $fax; ?></li>
                            <li><span>Email</span><a href="mailto:<?php echo $email ?>"><?php echo $email; ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Innerpage Contact End -->
