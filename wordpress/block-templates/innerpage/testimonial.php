<!-- Innerpage - Testimonial -->
<section class="testimonials testimonials-inner section-padding">
    <div class="container">
        <?php
            //Heading Variables
            $subheading = get_field('subheading');
            $heading    = get_field('heading');
        ?>
        <div class="text-center">
            <h5><?php echo $subheading; ?></h5>
            <h2><?php echo $heading; ?></h2>
        </div>
        <div class="testimonials-column">
            <div class="js-testimonials-slider fx-slider">
                <?php
                    //Testimonials Variables
                    $featured = get_field('testimonials');
                ?>
                <?php if( $featured ): ?>
                    <?php foreach( $featured as $feature ): ?>
                        <?php
                            //Featured Variables
                            $client_name = get_field('client_name', $feature);
                            $location    = get_field('location', $feature);
                            $content     = get_field('testimonial_content', $feature);
                            $logo        = get_field('brand_logo', $feature);
                        ?>
                        <div class="testimonials-item fx-slide">
                            <div class="testimonials-info">
                                <div class="testimonials-logo">
                                    <?php echo fx_get_image_tag($logo, 'img-responsive'); ?>
                                </div>
                                <p><?php echo $content; ?></p>
                                <h4><span><?php echo $client_name; ?></span></h4>
                                <h4><?php echo $location; ?></h4>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

        </div>
    </div>
</section>
<!-- Innerpage - Testimonial End -->
