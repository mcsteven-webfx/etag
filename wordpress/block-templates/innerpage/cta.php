<!-- Innerpages CTA -->
<section class="cta">
    <?php
        //CTA Variables
        $subheading = get_field('subheading', 'options');
        $heading    = get_field('heading', 'options');
        $image      = get_field('background_image', 'options');
        $button     = get_field('text_link', 'options');

        if( $button ):
            $button_url    = $button['url'];
            $button_title  = $button['title'];
            $button_target = $button['target'] ? $button['target'] : '_self';
        endif;
    ?>
    <div class="cta-bg">
        <?php echo fx_get_image_tag( $image, 'img-responsive' ); ?>
    </div>
    <div class="cta-wrapper">
        <div class="container">
            <div class="cta-info">
                <h5><?php echo $subheading; ?></h5>
                <h2><?php echo $heading; ?></h2>
                <a class="btn btn-secondary" href="<?php echo $button_url; ?>" target="<?php echo $button_target; ?>"><?php echo $button_title; ?></a>
            </div>
        </div>
    </div>
</section>
<!-- Innerpages CTA End -->
