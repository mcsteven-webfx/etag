<!-- Innerpage Image Button -->
<section class="image-buttons bg-blue section-padding">
    <?php
        //Content Variables
        $subheading = get_field('subheading');
        $heading    = get_field('heading');
        $button     = get_field('page_text_link');

        if( $button ):
            $button_url    = $button['url'];
            $button_title  = $button['title'];
            $button_target = $button['target'] ? $button['target'] : '_self';
        endif;
    ?>
    <div class="container">
        <div class="text-center">
            <h5><?php echo $subheading; ?></h5>
            <h2><?php echo $heading; ?></h2>
        </div>
    </div>
    <div class="image-slider-wrapper">
        <div class="image-button-slider fx-slider">
            <?php if( have_rows('image_buttons') ): ?>
                <?php while( have_rows('image_buttons') ): the_row(); ?>
                    <?php
                        //Image button Variables
                        $image       = get_sub_field('image');
                        $title       = get_sub_field('title');
                        $description = get_sub_field('description');
                        $link        = get_sub_field('text_link');

                        if( $link ):
                            $link_url    = $link['url'];
                            $link_title  = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        endif;
                    ?>
                    <div class="image-button-slide fx-slide">
                        <a class="image-button image-button--link" href="<?php echo $link_url; ?>">
                            <?php echo fx_get_image_tag($image, 'img-responsive image-button__img'); ?>
                            <div class="image-button__hover">
                                <h5 class="image-button__title"><?php echo $title; ?></h5>
                                <div class="image-button__hidden">
                                    <div class="image-button__description hidden-md-down">
                                        <p><?php echo $description; ?></p>
                                        <span class="btn btn-tertiary"><?php echo $link_title; ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <div class="container">
            <div class="image-bttn clearfix">

                <a href="<?php echo $button_url; ?>" class="btn btn-secondary" target="<?php echo $button_target; ?>"><?php echo $button_title; ?></a>

            </div>
        </div>
    </div>
</section>
<!-- Innerpage Image Button End -->
