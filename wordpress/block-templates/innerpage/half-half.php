<!-- Half and Half -->
<?php
    //Background Variable
    $bg = get_field('background');
?>
<section class="half-and-half flex-row image-block-right flex-opposite <?php echo $bg; ?>">
    <?php
        //Content Variables
        $image   = get_field('image');
        $content = get_field('wysiwyg');
    ?>
    <div class="half-and-half-image">
       <?php echo fx_get_image_tag( $image, 'img-responsive' ); ?>
    </div>
    <div class="half-and-half-text clearfix left">
       <div class="half-and-half-text__wrapper">
            <?php echo $content; ?>
       </div>
       <?php if( $bg == 'bg-pattern'): ?>
           <div class="pattern-half-and-half-bg">
               <?php echo fx_get_image_tag('https://etagtechnologies.webpagefxstage.com/wp-content/uploads/2022/02/pattern-wysiwyg.png', 'img-responsive'); ?>
           </div>
       <?php endif; ?>
    </div>

 </section>
 <!-- Half and Half End -->
