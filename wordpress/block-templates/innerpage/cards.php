<!-- Innerpage Cards -->
<section class="cards bg-blue section-padding">
    <div class="container">
        <?php
            //Heading Variables
            $subheading = get_field('subheading');
            $heading    = get_field('heading');
        ?>
        <div class="text-center">
            <h5><?php echo $subheading; ?></h5>
            <h2><?php echo $heading; ?></h2>
        </div>
        <div class="row card-flex">
            <?php if( have_rows('cards') ): ?>
                <?php while( have_rows('cards') ): the_row(); ?>
                    <?php
                        //Cards Variables
                        $icon        = get_sub_field('icon');
                        $title       = get_sub_field('title');
                        $description = get_sub_field('description');
                        $button      = get_sub_field('text_link');

                        if( $button ):
                            $button_url    = $button['url'];
                            $button_title  = $button['title'];
                            $button_target = $button['target'] ? $button['target'] : '_self';
                        else:
                            $button_url    = '#';
                        endif;
                    ?>
                    <div class="col-lg-3 col-xs-6 col-xxs-12 card-item">
                        <a class="card card--link" href="<?php echo $button_url; ?>">
                            <div class="card__top">
                                <?php if( $icon ): ?>
                                    <div class="card__img-wrap">
                                        <div class="card__icon">
                                            <?php echo fx_get_image_tag($icon, 'img-responsive'); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="card__details">
                                    <h5 class="card__title"><?php echo $title; ?></h5>
                                    <div class="card__description"><?php echo $description; ?></div>
                                </div>
                            </div>
                            <?php if( $button ): ?>
                                <div class="card__bottom">
                                    <button class="btn btn-tertiary" type="button"><?php echo $button_title; ?></button>
                                </div>
                            <?php endif; ?>
                        </a>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<!-- Innerpage Cards -->
