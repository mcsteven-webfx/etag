<!-- Innerpage FAQ -->
<section class="faq-section section-padding">
    <div class="container">
        <div class="row">
            <?php
                //Content Variables
                $content = get_field('wysiwyg');
            ?>
            <div class="col-md-6">
                <?php echo $content; ?>
            </div>
            <div class="col-md-6">
                <div class="fx-accordion js-accordion">
                    <div class="fx-accordion__panels">
                        <?php if( have_rows('faqs') ): ?>
                            <?php $counter = 0; ?>
                            <?php while( have_rows('faqs') ): the_row(); ?>
                                <?php
                                    //FAQ Variables
                                    $question = get_sub_field('question');
                                    $answer   = get_sub_field('answer');
                                ?>
                                <article class="fx-accordion__panel js-accordion-item" data-accordion-id="<?php echo $counter; ?>">
                                    <button class="fx-accordion__panel__toggle js-accordion-headline" type="button" data-accordion-id="<?php echo $counter; ?>"><?php echo $question; ?></button>

                                    <div class="fx-accordion__panel__content">
                                        <p><?php echo $answer; ?></p>
                                    </div>
                                </article>
                                <?php $counter++; ?>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- Innerpage FAQ End -->
