<!-- Brand Logo's -->
<section class="brand">
    <div class="container">
        <div class="brand-wrapper">
            <?php
                //Heading Variables
                $heading = get_field('heading');
            ?>
            <h5><?php echo $heading; ?></h5>
            <div class="js-brand-slider fx-slider">
                <?php if( have_rows('brand_logos') ): ?>
                    <?php while( have_rows('brand_logos') ): the_row(); ?>
                        <?php
                            //Slider Variables
                            $image = get_sub_field('image');
                            $link  = get_sub_field('link');
                        ?>
                        <div class="brand-item fx-slide">
                            <a href="<?php echo $link; ?>" target="_blank">
                                <?php echo fx_get_image_tag( $image, 'img-responsive' ); ?>
                            </a>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<!-- Brand Logo's End -->
