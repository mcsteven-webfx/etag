<!-- Homepage Process -->
<section class="process section-padding">
    <div class="process-pattern">
        <?php echo fx_get_image_tag( 'https://etagtechnologies.webpagefxstage.com/wp-content/uploads/2022/02/process-pattern.png', 'img-responsive' ); ?>
    </div>
    <div class="container">
        <div class="process-wrapper">
            <?php
                //Content Variable
                $content = get_field('wysiwyg');
            ?>
            <div class="process-content-column">
                <?php echo $content; ?>
            </div>
            <div class="process-steps-column">
               <div class="js-process-slider fx-slider">
                   <?php if( have_rows('process') ): ?>
                       <?php $counter = 1; ?>
                       <?php while( have_rows('process') ): the_row(); ?>
                           <?php
                               //Process Variables
                               $title       = get_sub_field('title');
                               $description = get_sub_field('description');
                           ?>
                           <div class="process-item fx-slide">
                               <div class="steps-column">
                                   <div class="steps-no"><?php echo $counter; ?></div>
                                   <h4><?php echo $title; ?></h4>
                                   <p><?php echo $description; ?></p>
                               </div>
                           </div>
                           <?php $counter++; ?>
                       <?php endwhile; ?>
                   <?php endif; ?>
               </div>
            </div>
        </div>
    </div>
</section>
<!-- Homepage Process End -->
