<!-- Homepage Quick Link -->
<section class="quick-info">
    <div class="container">
        <div class="quick-info-wrapper">
            <?php if( have_rows('statistics') ): ?>
                <?php while( have_rows('statistics') ): the_row(); ?>
                    <?php
                        //Quick Links Variables
                        $number = get_sub_field('number');
                        $symbol = get_sub_field('symbol');
                        $label  = get_sub_field('label');
                    ?>

                    <div class="quick-info-column">
                        <div class="quick-info-value"><span class="quick-info-count" data-value="<?php echo $number; ?>"><?php echo $number; ?></span><?php echo $symbol; ?></div>
                        <div class="quick-info-text">
                            <h4><?php echo $label; ?></h4>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<!-- Homepage Quick Link End -->
