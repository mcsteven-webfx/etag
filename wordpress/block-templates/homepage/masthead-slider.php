<!-- Homepage Masthead -->
<section class="masthead">
	<!-- Masthead Background -->
	<div class="masthead-img">
		<?php echo fx_get_image_tag( 'https://etagtechnologies.webpagefxstage.com/wp-content/uploads/2022/02/hero-image.jpg', 'img-responsive' ); ?>
	</div>
	<!-- Masthead Background End -->
	<!-- Masthead Slider -->
	<div class="masthead-slider fx-slider">
		<?php if( have_rows('slides') ): ?>
			<?php while( have_rows('slides') ): the_row(); ?>
				<div class="masthead-content fx-slide">
					<?php
						//Masthead Variables
						$subheading = get_sub_field('subheading');
						$heading    = get_sub_field('heading');
						$button     = get_sub_field('button');

						if( $button ):
							$button_url    = $button['url'];
							$button_title  = $button['title'];
							$button_target = $button['target'] ? $button['target'] : '_self';
						endif;
					?>
					<div class="container">
						<div class="masthead-text">
							<h5><?php echo $subheading; ?></h5>
							<h1><?php echo $heading; ?></h1>
							<a href="<?php echo $button_url; ?>" class="btn btn-primary" target="<?php echo $button_target; ?>"><?php echo $button_title; ?></a>
							<div class="forword-icon hidden-md-down">
								<?php echo fx_get_image_tag( 'https://etagtechnologies.webpagefxstage.com/wp-content/uploads/2022/02/forword-icon.png', 'img-responsive' ); ?>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<!-- Masthead Slider -->
</section>
<!-- Homepage Masthead End -->
