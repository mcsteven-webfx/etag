<!-- Homepage Product -->
<section class="product white-bg section-padding">
    <div class="container">
        <!-- Heading -->
        <div class="product-heading text-center">
            <?php
                //Heading Variables
                $subheading = get_field('subheading');
                $heading    = get_field('heading');
            ?>
            <h5><?php echo $subheading; ?></h5>
            <h2><?php echo $heading; ?></h2>
        </div>
        <!-- Heading End -->

        <!-- FAQ -->
        <div class="product-listings">
            <div class="js-product-slider-nav fx-slider">
                <?php if( have_rows('faqs') ): ?>
                    <?php while( have_rows('faqs') ): the_row(); ?>
                        <?php
                            //Slide Variables
                            $question = get_sub_field('question');
                        ?>
                        <div class="product-item fx-slide">
                            <h4><?php echo $question; ?></h4>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="js-product-slider-for fx-slider">
                <?php if( have_rows('faqs') ): ?>
                    <?php while( have_rows('faqs') ): the_row(); ?>
                        <?php
                            //Slider Variables
                            $image   = get_sub_field('image');
                            $content = get_sub_field('wysiwyg');
                        ?>
                        <div class="product-item-for fx-slide">
                            <div class="product-column">
                                <div class="product-image">
                                    <?php echo fx_get_image_tag($image, 'img-responsive'); ?>
                                </div>
                                <div class="product-info">
                                    <?php echo $content; ?>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
        <!-- FAQ End -->
    </div>
    <div class="product-patern">
        <?php echo fx_get_image_tag('https://etagtechnologies.webpagefxstage.com/wp-content/uploads/2022/02/product-patern.png', 'img-responsive'); ?>
    </div>
</section>
<!-- Homepage Product End -->
