<!-- Image Buttons -->
<section class="industry-section section-padding">
    <div class="container">
        <!-- Heading -->
        <div class="text-center">
            <?php
                //Heading Variables
                $subheading = get_field('subheading');
                $heading    = get_field('heading');
            ?>
            <h5><?php echo $subheading; ?></h5>
            <h2><?php echo $heading; ?></h2>
        </div>
        <!-- Heading End -->
        <!-- Slider -->
        <?php
            $total    = count( get_field('image_buttons') );
            $addClass = '';
        ?>

        <?php
            if( $total < 4 ):
                $addClass = 'industry-hide';
            endif;
        ?>
        <div class="industry-slider-wrapper">
            <div class="industry-slider fx-slider <?php echo $addClass; ?>">
                <?php if( have_rows('image_buttons') ): ?>
                    <?php $count = 0; ?>
                    <?php while( have_rows('image_buttons') ): the_row(); ?>
                        <?php
                            //Slider Variables
                            $image       = get_sub_field('image');
                            $title       = get_sub_field('title');
                            $link        = get_sub_field('link');
                            $description = get_sub_field('description');
                        ?>
                        <div class="industry-slide fx-slide">
                            <a class="industry-button--link" href="<?php echo $link; ?>">
                                <div class="image-box-wrapper">
                                    <div class="box-image">
                                        <?php echo fx_get_image_tag($image, 'img-responsive'); ?>
                                    </div>

                                    <div class="box-image-text">
                                        <h5><?php echo $title; ?></h5>
                                        <div class="industry-button__hidden">
                                            <p><?php echo $description; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php $count++; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="industry-bttn hidden-md-down clearfix">
                <?php
                    //Button Variables
                    $button = get_field('button');

                    if( $button ):
                        $button_url    = $button['url'];
                        $button_title  = $button['title'];
                        $button_target = $button['target'] ? $button['target'] : '_self';
                    endif;
                ?>
                <a href="<?php echo $button_url; ?>" class="btn btn-secondary" target="<?php echo $button_target; ?>"><?php echo $button_title; ?></a>
                <?php if( $count > 3 ): ?>
                    <div class="industry-slider-arrow">
                        <div class="left_button" onclick="jQuery('.industry-slider').slick('slickPrev');">
                            <i class="icon-Arrow-Left"></i>
                        </div>
                        <div class="right_button nitro-offscreen" onclick="jQuery('.industry-slider').slick('slickNext');">
                            <i class="icon-Arrow-Right"></i>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <!-- Slider End -->
    </div>
</section>
<!-- Image Buttons End -->
