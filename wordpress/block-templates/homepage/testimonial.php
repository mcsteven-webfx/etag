<!-- Homepage Testimonial -->
<section class="testimonials section-padding">
    <div class="container">
        <div class="testimonials-column">
            <?php
                //Testimonial Variables
                $featured    = get_field('featured_post');
                $client_name = get_field('client_name', $featured);
                $location    = get_field('location', $featured);
                $content     = get_field('testimonial_content', $featured);
                $image       = get_post_thumbnail_id($featured);
                $logo        = get_field('brand_logo', $featured);
            ?>

            <?php if( $featured ): ?>
                <div class="testimonials-image">
                    <?php echo fx_get_image_tag($image, 'img-responsive'); ?>
                </div>
                <div class="testimonials-info">
                    <div class="testimonials-logo">
                        <?php echo fx_get_image_tag($logo, 'img-responsive'); ?>
                    </div>
                    <?php echo $content; ?>
                    <h4><span><?php echo $client_name; ?></span></h4>
                    <h4><?php echo $location; ?></h4>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<!-- Homepage Testimonial End -->
