<?php

/**
 * $template note:
 *
 * Block names should be prefixed with acf/. So if the name you specified in
 * fx_register_block is 'your-block-name', the name you should use here is
 * 'acf/your-block-name'
 */

$template = [
	['acf/homepage-masthead-slider'],
	['acf/homepage-quick-info'],
	['acf/homepage-product'],
	['acf/homepage-image-buttons'],
	['acf/homepage-brand-logos'],
	['acf/innerpage-half-half'],
	['acf/homepage-testimonial'],
	['acf/homepage-process'],
	['acf/innerpage-cta'],
];

?>

<div>
    <InnerBlocks template="<?php echo esc_attr( wp_json_encode( $template ) )?>" templateLock="all" />
</div>
