<?php get_header(); ?>

<?php get_template_part('partials/masthead'); ?>

<div class="container section-margins">
    <!-- Image Buttons -->
    <section class="imgbtns-404">
        <div class="row">
            <div class="col-xxs-12">
                <h3>Explore one of these pages instead:</h3>
            </div>
            <?php if( have_rows('image_buttons', 'option') ): ?>
                <?php while( have_rowS('image_buttons', 'option') ): the_row(); ?>
                    <?php
                    //404 Variables
                    $image = get_sub_field('image', 'option');
                    $link  = get_sub_field('title_link', 'option');

                    if($link):
                        $link_url    = $link['url'];
                        $link_title  = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                    endif;
                    ?>
                    <div class="col-xxs-12 col-sm-6 col-md-3">
                        <a href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>" class="imgbtns__link">
                            <?php echo fx_get_image_tag($image, 'img-responsive'); ?>
                            <h5><?php echo $link_title; ?></h5>
                        </a>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </section>
    <!-- Image Buttons End -->
    <!-- Other Links -->
    <section class="links-404">
        <div class="row">
            <div class="col-xxs-12 col-md-6">
                <div class="search-404">
                    <h4>Or, try searching our site:</h4>
                    <?php get_search_form(); ?>
                </div>
            </div>
            <div class="col-xxs-12 col-md-6">
                <div class="contact-404">
                    <h4>Still can't find what you're looking for?</h4>
                    <a href="#" class="btn btn-primary">Contact Us Today!</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Other Links End -->
</div>

<?php get_footer();
