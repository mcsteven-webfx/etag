<?php
/**
 * If your form is generated using get_search_form() you do not need to do this,
 * as SearchWP Live Search does it automatically out of the box
 */
?>
<form action="" method="get" class="search-div desktop-menu__search">
    <div class="container clearfix">
        <div class="search-content">
            <input type="text" name="s" id="s" value="" data-swplive="true" placeholder="Search the eTag Technology site…">
            <button type="submit"><i class="icon-Search-1"></i></button>
        </div>
    </div>
</form>
