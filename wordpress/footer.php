        <footer class="page-footer">
            <div class="container">
                <div class="footer-wrapper">
                    <div class="footer-left-column">
                        <!-- Footer Logo -->
                        <div class="footer-logo">
                            <a href="<?php echo $home_url; ?>">
                                <?php echo fx_get_image_tag( 'https://etagtechnologies.webpagefxstage.com/wp-content/uploads/2022/02/logo.png', 'img-responsive' ); ?>
                            </a>
                        </div>
                        <!-- Footer Logo End -->

                        <!-- Secondary Menu -->
                        <div class="footer-secondary-menu">
                            <?php
                                //Secondary Menu Variables
                                $credits = get_field( 'site_credits','options' );
                                $sitemap = get_field( 'sitemap','options' );
                                $policy  = get_field( 'privacy_policy','options' );

                                if( $credits ):
                                    $credits_url    = $credits['url'];
                                    $credits_title  = $credits['title'];
                                    $credits_target = $credits['target'] ? $credits['target'] : '_self';
                                endif;

                                if( $sitemap ):
                                    $sitemap_url    = $sitemap['url'];
                                    $sitemap_title  = $sitemap['title'];
                                    $sitemap_target = $sitemap['target'] ? $sitemap['target'] : '_self';
                                endif;

                                if( $policy ):
                                    $policy_url    = $policy['url'];
                                    $policy_title  = $policy['title'];
                                    $policy_target = $policy['target'] ? $policy['target'] : '_self';
                                endif;
                            ?>
                            <ul>
                                <li><a href="<?php echo $credits_url; ?>" target="<?php echo $credits_target; ?>"><?php echo $credits_title; ?></a></li>
                                <li><a href="<?php echo $sitemap_url; ?>" target="<?php echo $sitemap_target; ?>"><?php echo $sitemap_title; ?></a></li>
                                <li><a href="<?php echo $policy_url; ?>" target="<?php echo $policy_target; ?>"><?php echo $policy_title; ?></a></li>
                                <li class="copyright">Copyright © <?php echo date("Y"); ?>. All Rights Reserved.</li>
                            </ul>
                        </div>
                        <!-- Secondary Menu End -->
                    </div>

                    <div class="footer-right-column">
                        <!-- Site Details -->
                        <div class="footer-col footer-contact-col">
                            <?php
                                //Site Details Variables
                                $address    = fx_get_client_address();
                                $email      = fx_get_client_email( true );
                                $phone      = fx_get_client_phone_number();
                                $phone_link = fx_get_client_phone_number( true );
                            ?>
                            <h3>Contact us</h3>
                            <p><i class="icon-Location"></i> <?php echo $address; ?></p>
                            <p><i class="icon-Mail"></i> <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
                            <p class="footer-phone"><i class="icon-Phone"></i> <a href="tel:<?php echo $phone_link; ?>"><?php echo $phone; ?></a></p>
                        </div>
                        <!-- Site Details End -->
                        <!-- Menu -->
                        <div class="footer-col quick-links-col hidden-sm hidden-md">
                            <h3>QUICK LINKS</h3>
                            <?php
                            wp_nav_menu(
                                [
                                    'theme_location'    => 'footer_menu',
                                    'depth'             => 1,
                                ]
                            );
                            ?>
                        </div>
                        <!-- Menu End -->
                        <!-- Social Media -->
                        <div class="footer-col footer-social-media-col">
                            <div class="social-media">
                                <?php
                                    //Social Media Variables
                                    $facebook  = get_field( 'facebook','option' );
                                    $twitter   = get_field( 'twitter','option' );
                                    $linkedin  = get_field( 'linkedin','option' );
                                    $instagram = get_field( 'instagram','option' );
                                ?>
                                <ul>
                                    <li><a href="<?php echo $facebook; ?>"><i class="icon-Facebook"></i></a></li>
                                    <li><a href="<?php echo $twitter; ?>"><i class="icon-Twitter"></i></a></li>
                                    <li><a href="<?php echo $linkedin; ?>"><i class="icon-Linkedin"></i></a></li>
                                    <li><a href="<?php echo $instagram; ?>"><i class="icon-Instagram"></i></a></li>
                                </ul>
                            </div>
                            <div class="back-top">
                                <a href="javascript:void(0);" id="back-to-top" class="back-to-top btn btn-primary"> <span> BACK TO TOP  <i class="icon-arrow-up"></i></span> </a>
                            </div>
                        </div>
                        <!-- Social Media End -->
                    </div>
                </div>
            </div>
        </footer>

        <?php wp_footer(); ?>
    </body>
</html>
