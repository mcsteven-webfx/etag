var FX = ( function( FX, $ ) {


	$( () => {
		FX.ImageButtonSlider.init()
	})


	FX.ImageButtonSlider = {
		$slider: null,

		init() {
			$('.image-button-slider').slick({
				dots: false,
				infinite: true,
				speed: 300,
				slidesToShow: 3,
				centerMode: true,
				arrows: true,
				centerPadding: '260px',
				responsive: [
					{
					  breakpoint: 1200,
					  settings: {
						centerMode: true,
						centerPadding: '60px',
						slidesToShow: 3
					  }
					},
					{
						breakpoint: 1025,
						settings: {
						  centerMode: true,
						  centerPadding: '60px',
						  slidesToShow: 2
						}
					  },
					{
					  breakpoint: 768,
					  settings: {
						centerMode: true,
						centerPadding: '40px',
						slidesToShow: 1
					  }
					}
				  ]
			});

		
		}
	}

	

	return FX

} ( FX || {}, jQuery ) )