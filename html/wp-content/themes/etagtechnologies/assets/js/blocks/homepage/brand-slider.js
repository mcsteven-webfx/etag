var FX = ( function( FX, $ ) {


	$( () => {
		FX.BrandSlider.init()
	})


	FX.BrandSlider = {
		$slider: null,

		init() {
			$('.js-brand-slider').slick({
				infinite: true,
				speed: 300,
				dots: false,
				arrows: true,
				slidesToShow: 4,
				responsive: [
					{
						breakpoint: 9999,
						settings: 'unslick'
					},
					{
						breakpoint: 1200,
						settings: {
							slidesToShow: 2,
							arrows: true,
						}
					},
					{
						breakpoint: 767,
						settings: {
							slidesToShow: 1,
							arrows: true,
						}
					},
				  ]
			});	


		},

		applySlick() {
            this.$slider.slick( {
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
            });
		}
	}

	

	return FX

} ( FX || {}, jQuery ) )