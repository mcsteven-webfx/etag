var FX = ( function( FX, $ ) {


	$( () => {
		FX.ProcessSlider.init()
	})


	FX.ProcessSlider = {
		$slider: null,

		init() {
			$('.js-process-slider').slick({
				infinite: true,
				speed: 300,
				dots: false,
				arrows: true,
				slidesToShow: 4,
				responsive: [
					{
						breakpoint: 9999,
						settings: 'unslick'
					},
					{
						breakpoint: 1200,
						settings: {
							slidesToShow: 3,
							arrows: true,
							focusOnSelect: true,
							centerMode: true,
							veriableWidth: true,
							centerPadding: '0px',
							adaptiveHeight: true,
						}
					},
					{
						breakpoint: 767,
						settings: {
							slidesToShow: 1,
							arrows: true,
							centerMode: false,
							veriableWidth: false,
						}
					},
				  ]
			});	


		},

		applySlick() {
            this.$slider.slick( {
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
            });
		}
	}

	

	return FX

} ( FX || {}, jQuery ) )