var FX = ( function( FX, $ ) {


	$( () => {
		FX.ProductSlider.init()
	})


	FX.ProductSlider = {
		$slider: null,

		init() {
			$('.js-product-slider-nav').slick({
				slidesToShow: 4,
				slidesToScroll: 1,
				arrows: false,
				dots: false,
				focusOnSelect: true,
				asNavFor: '.js-product-slider-for',
				responsive: [
					{
			 			breakpoint: 1200,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							arrows: true,
						}
			 		},
				]
			});

			$('.js-product-slider-for').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				asNavFor: '.js-product-slider-nav',
				dots: false,
				arrows: false,
				fade: true,
				arrows: false,
				focusOnSelect: true
			});

		},

		applySlick() {
            this.$slider.slick( {
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
            });
		}
	}

	

	return FX

} ( FX || {}, jQuery ) )